function is_defined(v) {
  return typeof(v) != 'undefined';
}

// Display an amil
function de(email, opt) {
  if (typeof(opt) == 'undefined') opt = { };
  var addr = be(email);
  if (!is_defined(opt.linked) || opt.linked) {
    var label = (is_defined(opt.label) ? opt.label : addr);
    return '<A HREF="mailto:' + addr + '">' + label + '</A>';
  } else
    return addr;
}

// Build an address
function be(email) {
  return email + '@debear.uk';
}

