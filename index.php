<?php
  $_INFO = array('meta_descrip' => 'We apologise whilst we give the hamsters a rest, and perform some essential maintenance on the site.', 'meta_img' => 'http://' . $_SERVER['SERVER_NAME'] . '/images/meta/debear.png', 'title' => 'We are undergoing some essential site maintenance.', 'logo' => 'default_logo.png', 'twitter' => 'DeBearSupport', 'reasons' => array('what' => 'Some unforeseen remedial work needs to be performed on this site, which is best performed with the site down. (Translation: something&#39;s unexpectedly broken and we&#39;re working out how best to fix it without the problem escalating...)', 'why' => 'Taking the whole site offline has been a bit of a last resort, however we felt it was the safest way to complete this work without any further risk to the site or its data.', 'when' => 'The work will usually be completed within an hour or two, though unfortunately due to the unknown nature of the problem it may take longer than this. We will endeavour to keep this page and our Twitter feed updated when we have relevant progress updates.'));
  // Load reason information as to why we're in a specific maintenance period?
  if (isset($_GET['reason']) && ($file = 'reasons/' . $_GET['reason']) && file_exists($file)) {
    $ext = file_get_contents($file);
    foreach (explode("\n", $ext) as $row)
      // Ensure the row matches our format (includes our split char with at least one char before it)
      if (trim($row) && substr($row, 0, 1) != '#' && strpos($row, ':', 1) !== false) {
        list($key, $value) = explode(':', $row, 2);
        $key = strtolower($key);
        if (isset($_INFO['reasons'][$key]))
          $_INFO['reasons'][$key] = $value;
      }
  }
  
  // Mobile site logo checks
  $_INFO['logo_mobile'] = str_replace('.', '_mobile.', $_INFO['logo']);
  if (!file_exists('images/logos/' . $_INFO['logo_mobile']))
    $_INFO['logo_mobile'] = false;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><HTML>
<HEAD>
  <META NAME="Description" CONTENT="<?=$_INFO['meta_descrip']?>" />
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8" />
  <META NAME="viewport" CONTENT="width=device-width,initial-scale=1.0" />
  <META NAME="twitter:card" CONTENT="summary" />
  <META NAME="twitter:title" CONTENT="<?=$_INFO['title']?>" />
  <META NAME="twitter:description" CONTENT="<?=$_INFO['meta_descrip']?>" />
<?php if (isset($_INFO['meta_twitter_handle'])) { ?>
  <META NAME="twitter:site" CONTENT="<?=$_INFO['meta_twitter_handle']?>" />
  <META NAME="twitter:creator" CONTENT="<?=$_INFO['meta_twitter_handle']?>" />
<?php } ?>
  <META NAME="twitter:image" CONTENT="<?=$_INFO['meta_img']?>" />
  <META PROPERTY="og:title" CONTENT="<?=$_INFO['title']?>" />
  <META PROPERTY="og:type" CONTENT="article" />
  <META PROPERTY="og:url" CONTENT="<?=$_SERVER['SERVER_NAME']?><?=$_SERVER['REQUEST_URI']?>" />
  <META PROPERTY="og:image" CONTENT="<?=$_INFO['meta_img']?>" />
  <META PROPERTY="og:description" CONTENT="<?=$_INFO['meta_descrip']?>" /> 
  <META PROPERTY="og:site_name" CONTENT="DeBear.uk" />
  <TITLE><?=$_INFO['title']?></TITLE>
  <SCRIPT TYPE="text/javascript" SRC="/s/site.js"></SCRIPT>
  <LINK REL="stylesheet" TYPE="text/css" HREF="/s/site.css" />
</HEAD>
<BODY>
  <HEADER>
<?php if ($_INFO['logo_mobile']) { ?>
    <DIV CLASS="logo">
      <IMG CLASS="desktop" ALT="Logo" SRC="/i/l/<?=$_INFO['logo']?>">
      <IMG CLASS="mobile" ALT="Logo" SRC="/i/l/<?=$_INFO['logo_mobile']?>">
    </DIV>
<?php } else { ?>
    <IMG ALT="Logo" SRC="/i/l/<?=$_INFO['logo']?>">
<?php } ?>
  </HEADER>
  <MAIN>
    <H1><?=$_INFO['title']?></H1>
    <P>Please bear with us (sorry, we had to...) whilst we complete this work.</P>
    <DL CLASS="info">
      <DT>What are you doing?</DT>
      <DD><?=$_INFO['reasons']['what']?></DD>
      <DT>Why are you doing this?</DT>
      <DD><?=$_INFO['reasons']['why']?></DD>
      <DT>When do you think you will be finished?</DT>
      <DD><?=$_INFO['reasons']['when']?></DD>
      <DT>How do I contact you if this message still appears after the time you think you will be finished?</DT>
      <DD>Firstly, thank you for your patience. The best ways to get hold of us to berate our poor progress updates are either via email (<SCRIPT TYPE="text/javascript">document.write(de('support'));</SCRIPT>) or via Twitter (<A HREF="https://twitter.com/<?=$_INFO['twitter']?>" TARGET="_blank">@<?=$_INFO['twitter']?></A>).</DD>
    </DL>
  </MAIN>
  <FOOTER>
    <SPAN CLASS="twitter">
      <A HREF="https://twitter.com/<?=$_INFO['twitter']?>" CLASS="default twitter-follow-button" DATA-SHOW-COUNT="false" DATA-SIZE="large" DATA-WIDTH="200px" DATA-ALIGN="right">Follow @<?=$_INFO['twitter']?></a>
      <SCRIPT TYPE="text/javascript">!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</SCRIPT>
    </SPAN>
  </FOOTER>
</BODY>
</HTML>

